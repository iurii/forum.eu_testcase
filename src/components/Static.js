import React from 'react';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import theme from './../theme'
import static1 from './../Static1.png'
import static2 from './../Static2.png'

function Static(props) {
  return (
    <Container fixed>
      <Grid container justify="center">
        <img src={static1} alt="Static content 1" style={ useMediaQuery(theme.breakpoints.up('lg')) ? { width: '150%' } : { width: '100%' } } />
        <img src={static2} alt="Static content 2" style={ useMediaQuery(theme.breakpoints.up('lg')) ? { width: '150%' } : { width: '100%' } } />
      </Grid>
    </Container>
  );
}

export default Static;
