import React from 'react';
import Grid from '@material-ui/core/Grid';
import Search from './Search/Search'
import Register from './Register/Register'

function ActionArea(props) {
  return (
    <Grid container item xs={6} justify="flex-end">
      <Search />
      <Register />
    </Grid>
  );
}

export default ActionArea;
