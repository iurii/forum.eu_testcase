import React from 'react';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import profile from './profile.svg';

function Register(props) {
  const RegisterButton = withStyles(() => ({
    root: {
      width: '127.92px',
      height: '46px',
      padding: '0px',
      background: '#0096FF 0% 0% no-repeat padding-box',
      boxShadow: '0px 3px 6px #0000001A',
      borderRadius: '54px',
      font: 'Bold 16px/16px Roboto',
      letterSpacing: '0px',
      color: '#FFFFFF',
      textTransform: 'none',
      '&:hover': {
        background: '#0096FF 0% 0% no-repeat padding-box',
      },
    },
  }))(Button);

  return (
    <RegisterButton startIcon={<img src={profile} alt="profile" />}>
      Register
    </RegisterButton>
  );
}

export default Register;
