import React from 'react';
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import { ThemeProvider } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import ActionArea from './components/ActionArea'
import Debates from './components/Debates/Debates'
import Header from './components/Header'
import Logo from './components/Logo/Logo'
import Static from './components/Static'
import theme from './theme'

function App() {
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Container style={ useMediaQuery(theme.breakpoints.up('md')) ? { padding: '0px 175px' } : {} }>
        <Header>
          <Logo />
          <ActionArea />
          <Debates />
        </Header>
        <Static />
      </Container>
    </ThemeProvider>
  );
}

export default App;
