# Coding challenge for [Forum.eu](https://forum.eu/)

## Installation and running
1. Install Node.js environment with Node version 12.16.3 or higher. More on this [here](https://nodejs.org/en/download/package-manager/). The following steps assume using [npm](https://www.npmjs.com/)
2. Install the dependencies: `npm install`
3. Build for production and run:

        npm run build
        npm install -g serve
        serve -s build

4. Or one can simply run the development server: `npm start`

## Tests
To run the set of smoke tests: `npm run test`
