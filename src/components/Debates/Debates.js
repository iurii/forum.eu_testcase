import React, { useEffect, useState } from 'react';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import { withStyles } from '@material-ui/styles';
import theme from './../../theme'
import arrowDown from './arrow-down.svg';

const debates = ['Brexit', 'Climate', 'Copyright', 'Migration', 'Deb on hover', 'Debate X', 'Debate Y', 'Debate Z', 'Debate A'];
const styles = {
  'debates': {
    padding: '20px 0px',
  },
  'debate': {
    textAlign: 'left',
    font: 'Bold 14px/20px Roboto',
    letterSpacing: 0,
    '& a': {
      color: '#132F48',
      textDecoration: 'none',
      opacity: 0.5,
      '&:visited': {
        color: '#132F48',
        opacity: 0.5,
      },
      '&:hover': {
        color: '#132F48',
        opacity: 1,
      },
      '& img': {
        marginLeft: 3,
        verticalAlign: 'middle',
      },
    },
  },
  'debatesAnimateOut': {
    margin: '-56px 0 0 52px',
    padding: '23px 0px 0px',
    transition: 'margin 1s, padding 1s',
  },
  'debatesAnimateIn': {
    margin: 0,
    padding: '20px 0px',
    transition: 'margin 1s, padding 1s',
  },
  'debateAnimateOut': {
    visibility: 'hidden',
    opacity: 0,
    transition: 'visibility 1s, opacity 1s',
  },
  'debateAnimateIn': {
    visibility: 'visible',
    opacity: 1,
    transition: 'visibility 1s, opacity 1s',
  },
};

function Debate(props) {
  return (
    <Box className={props.classes['debate']}>
      <a href={props.link}>{props.children}</a>
    </Box>
  );
}

function Debates(props) {
  const [cls, setCls] = useState({
    debates: '',
    debate: '',
  })

  function onScroll(event) {
    if(window.innerWidth < theme.breakpoints.values['lg']) {
      return;
    }
    if(window.scrollY > 10) {
      setCls(state => ({
        debates: props.classes.debatesAnimateOut,
        debate: props.classes.debateAnimateOut,
      }));
    } else {
      setCls(state => ({
        debates: props.classes.debatesAnimateIn,
        debate: props.classes.debateAnimateIn,
      }));
    }
  }

  useEffect(() => {
    if (window) {
      window.addEventListener('scroll', onScroll);
    }
    return function cleanup() {
      window.removeEventListener('scroll', onScroll);
    };
  });

  return (
    <Grid container item sm={12} className={`${props.classes.debates} ${cls.debates}`} justify="space-between">
      <Debate link="#all" classes={props.classes} className="dropdown">
        All A-Z
        <img src={arrowDown} alt="Expand" />
      </Debate>
      {debates.map((debate, index) =>
        <Hidden
          {...(index > 6 ? {mdDown: true} : (index > 5 ? {smDown: true} : {xsDown: true}))}
          key={debate}
        >
          <Grid item className={index > 5 ? cls.debate : ''}>
            <Debate link={'#' + index.toString()} classes={props.classes}>
              {debate}
            </Debate>
          </Grid>
        </Hidden>
      )}
    </Grid>
  );
}

export default withStyles(styles)(Debates);
