import React, { useEffect, useState } from 'react';
import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputAdornment from '@material-ui/core/InputAdornment';
import { withStyles } from '@material-ui/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import theme from './../../theme'
import search from './search.svg';

const styles = {
  'formControl': {
    width: 189,
    height: 36,
    background: '#E6ECF0 0% 0% no-repeat padding-box',
    borderRadius: 60,
  },
  'searchIcon': {
    width: 14,
    height: 14,
    margin: '11px 8px 11px 18px',
  },
  'input': {
    '&::placeholder': {
      font: 'Regular 16px/16px Roboto',
      letterSpacing: 0,
      color: '#B8C0C8',
      opacity: 1,
    },
  },
  'formControlAnimateOut': {
    width: 36,
    transition: 'width 1s',
  },
  'formControlAnimateIn': {
    width: 189,
    transition: 'width 1s',
  },
  'searchIconAnimateOut': {
    margin: 11,
    transition: 'margin 1s',
  },
  'searchIconAnimateIn': {
    margin: '11px 8px 11px 18px',
    transition: 'margin 1s',
  },
};

function Search(props) {
  const [cls, setCls] = useState({
    formControl: '',
    searchIcon: '',
  })

  function onScroll(event) {
    if(window.innerWidth < theme.breakpoints.values['lg']) {
      return;
    }
    if(window.scrollY > 10) {
      setCls(state => ({
        formControl: props.classes.formControlAnimateOut,
        searchIcon: props.classes.searchIconAnimateOut,
      }));
    } else {
      setCls(state => ({
        formControl: props.classes.formControlAnimateIn,
        searchIcon: props.classes.searchIconAnimateIn,
      }));
    }
  }

  useEffect(() => {
    if (window) {
      window.addEventListener('scroll', onScroll);
    }
    return function cleanup() {
      window.removeEventListener('scroll', onScroll);
    };
  });

  return (
    <Grid container item xs={6} alignItems="center" justify="flex-end" style={{ paddingRight: 20 }}>
      <FormControl
        className={`${props.classes['formControl']} ${cls.formControl}`}
        style={ useMediaQuery(theme.breakpoints.down('sm')) ? { width: 36 } : {} }
      >
        <Input
          placeholder="Search"
          classes={{input: props.classes['input']}}
          startAdornment={
            <InputAdornment
              position="start"
              className={`${props.classes['searchIcon']} ${cls.searchIcon}`}
              style={ useMediaQuery(theme.breakpoints.down('sm')) ? { margin: 11 } : {} }
            >
              <img src={search} alt="search" />
            </InputAdornment>
          }
          disableUnderline={true}
        />
      </FormControl>
    </Grid>
  );
}

export default withStyles(styles)(Search);
