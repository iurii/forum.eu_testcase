import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

test('renders logo icon', () => {
  const { getByAltText } = render(<App />);
  const logoIcon = getByAltText(/logo icon/i);
  expect(logoIcon).toHaveAttribute('src', 'forum-dots.svg');
});

test('renders logo text', () => {
  const { getByAltText } = render(<App />);
  const logoText = getByAltText(/logo text/i);
  expect(logoText).toHaveAttribute('src', 'forum-logo-text.svg');
});

test('renders search', () => {
  const { getByPlaceholderText } = render(<App />);
  const search = getByPlaceholderText(/Search/i);
  expect(search).toBeInTheDocument();
});

test('renders search icon', () => {
  const { getByAltText } = render(<App />);
  const searchIcon = getByAltText(/search/i);
  expect(searchIcon).toHaveAttribute('src', 'search.svg');
});

test('renders register', () => {
  const { getByRole } = render(<App />);
  const register = getByRole('button');
  expect(register).toHaveTextContent('Register');
});

test('renders register icon', () => {
  const { getByAltText } = render(<App />);
  const registerIcon = getByAltText('profile');
  expect(registerIcon).toHaveAttribute('src', 'profile.svg');
});

test('renders dropdown debates', () => {
  const { getByText } = render(<App />);
  const dropdown = getByText(/All A-Z/i);
  expect(dropdown).toBeInTheDocument();
});

test('renders dropdown debates icon', () => {
  const { getByAltText } = render(<App />);
  const dropdownIcon = getByAltText(/Expand/i);
  expect(dropdownIcon).toHaveAttribute('src', 'arrow-down.svg');
});

test('renders first static content', () => {
  const { getByAltText } = render(<App />);
  const staticContent1 = getByAltText(/Static content 1/i);
  expect(staticContent1).toHaveAttribute('src', 'Static1.png');
});

test('renders second static content', () => {
  const { getByAltText } = render(<App />);
  const staticContent2 = getByAltText(/Static content 2/i);
  expect(staticContent2).toHaveAttribute('src', 'Static2.png');
});
