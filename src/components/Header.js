import React from 'react';
import Grid from '@material-ui/core/Grid';

function Header(props) {
  return (
    <header style={{ paddingTop: 22 }}>
      <Grid container style={{ padding: '6px 0px' }}>
        {props.children}
      </Grid>
    </header>
  );
}

export default Header;
