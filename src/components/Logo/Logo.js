import React, { useEffect, useState } from 'react';
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/styles';
import theme from './../../theme'
import forumDots from './forum-dots.svg';
import forumLogoText from './forum-logo-text.svg';

const styles = {
  'animateOut': {
    marginTop: '-200px',
    transition: 'margin-top 1s',
  },
  'animateIn': {
    marginTop: 0,
    transition: 'margin-top 1s',
  },
};

function Logo(props) {
  const [cls, setCls] = useState('')

  function onScroll(event) {
    if(window.innerWidth < theme.breakpoints.values['lg']) {
      return;
    }
    var logoText = document.getElementById("logoText");
    if(window.scrollY > 10 && logoText !== null) {
      setCls(props.classes.animateOut);
    } else if (logoText !== null) {
      setCls(props.classes.animateIn);
    }
  }

  useEffect(() => {
    if (window) {
      window.addEventListener('scroll', onScroll);
    }
    return function cleanup() {
      window.removeEventListener('scroll', onScroll);
    };
  });

  return (
    <Grid container item xs={6} alignItems="center">
      <img src={forumDots} alt="logo icon" />
      <img id="logoText" src={forumLogoText} alt="logo text" className={cls} style={{ marginLeft: 5 }}/>
    </Grid>
  );
}

export default withStyles(styles)(Logo);
